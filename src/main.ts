import { exec } from 'child_process';

export default function createWidget(widgetfolder: string, isvertical: boolean) {
    let menubtn = document.createElement("div");
    menubtn.style.position = "absolute";
    menubtn.style.top = "0";
    menubtn.style.left = "0";
    if (!isvertical) {
        menubtn.style.bottom = "0";
        menubtn.style.right = "1px";
    } else {
        menubtn.style.bottom = "1px";
        menubtn.style.right = "0";
    }
    menubtn.style.cursor = "pointer";
    menubtn.className = "menubtncontainer";

    let splitter = document.createElement("div");
    splitter.style.position = "absolute";
    if (!isvertical) {
        splitter.style.top = "4px";
        splitter.style.right = "0px";
        splitter.style.bottom = "4px";
        splitter.style.width = "1px";
    } else {
        splitter.style.left = "4px";
        splitter.style.right = "4px";
        splitter.style.bottom = "0px";
        splitter.style.height = "1px";
    }
    splitter.style.backgroundColor = "rgba(190, 190, 204, 0.58)";

    let styles = document.createElement("style");
    styles.innerHTML = `.menubtncontainer:hover img {
        opacity: 0.95 !important;
    }`;

    var menuicon = document.createElement("img");
    menuicon.src = widgetfolder + "/src/icon.svg";
    menuicon.style.position = "absolute";
    menuicon.style.top = "50%";
    menuicon.style.left = "50%";
    menuicon.style.transform = "translate(-50%, -50%)";
    menuicon.style.opacity = "0.58";
    menuicon.style.transition = "0.2s";

    menubtn.appendChild(styles);
    menubtn.appendChild(splitter);
    menubtn.appendChild(menuicon);

    menubtn.onclick = function() {
        exec("instantlauncher");
    }

    return menubtn;
}